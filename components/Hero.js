import Container from "./Container"
import Button from "./Button"

const Hero = () => {
  return (
    <div className="hero">
      <Container>
        <div className="hero__content">
          <h3 className="hero__subtitle">Оригинальные производители</h3>
          <h1 className="hero__title">
            Премиум корма для ваших{` `}
            <br className="hero__br" />
            домашних любимцев
          </h1>
          <Button href="#form">Узнать больше</Button>
          <img src="/static/img/banner.png" alt="подарок каждому покупателю" />
        </div>
      </Container>
      <style jsx>{`
        .hero {
          width: 100%;
          height: 100vh;
          background-image: linear-gradient(
              to bottom,
              rgba(40, 84, 40, 0.6) 0%,
              rgba(1, 19, 3, 0.6) 100%
            ),
            url(/static/img/hero.png);
          background-size: cover;
          background-position: center center;
          display: flex;
          flex-direction: column;
          justify-content: center;
        }
        .hero__title {
          margin-top: 0.5rem;
          margin-bottom: 1rem;
          color: #fff;
          text-transform: uppercase;
          font-size: 50px;
        }
        .hero__subtitle {
          color: #34a29c;
          text-transform: uppercase;
          font-size: 25px;
          margin-top: 10rem;
          margin-bottom: 0;
        }
        .hero__content {
          animation: fadein 0.5s ease-in;
          position: relative;
        }
        .hero__content img {
          position: absolute;
          bottom: -170px;
          right: -40px;
        }
        @keyframes fadein {
          0% {
            transform: translateY(40px);
            opacity: 0;
          }
          100% {
            transform: translateY(0);
            opacity: 1;
          }
        }
        @media (max-width: 640px) {
          .hero__subtitle {
            font-size: 20px;
          }
          .hero__title {
            font-size: 40px;
          }
          .hero__content img {
            max-width: 200px;
            height: auto;
            right: -30px;
            bottom: -120px;
          }
        }
        @media (min-width: 640px) and (max-width: 920px) {
          .hero__content img {
            max-width: 250px;
            height: auto;
            bottom: -130px;
          }
          .hero__br {
            display: none;
          }
        }
      `}</style>
    </div>
  )
}

export default Hero
