import Link from "next/link"
import Container from "./Container"
import SocialIcon from "./SocialIcon"

export default ({ id }) => (
  <div className="footer" id={id}>
    <Container>
      <div className="footer__content">
        <div className="footer__column">
          <Link href="/">
            <a className="footer__logo">
              <div className="left">
                <img src="/static/logo.png" alt="логотип 'Сытый любимчик'" />
              </div>
              <div className="right">
                <div>Сытый</div>
                <div>любимчик</div>
              </div>
            </a>
          </Link>
          <div className="footer__location">
            <div className="footer__loc-icon">
              <i className="fas fa-phone-alt"></i>
            </div>
            <div className="footer__loc-phone">
              <a href="tel:+79127542612">8 (912) 754-26-12</a>
            </div>
          </div>
          <div className="footer__location">
            <div className="footer__loc-icon">
              <i className="fas fa-map-marker-alt"></i>{" "}
            </div>
            <div>
              г. Ижевск,
              <br />
              ул. Кунгурцева, 13
            </div>
          </div>
        </div>
        <div className="footer__column">
          <div className="footer-menu">
            <Link href="#manufacturers">
              <a>Производители</a>
            </Link>
            <Link href="#for-whom">
              <a>Для кого</a>
            </Link>
            <Link href="#reviews">
              <a>Отзывы</a>
            </Link>
            <Link href="#footer">
              <a>Контакты</a>
            </Link>
          </div>
        </div>
        <div className="footer__column">
          <h5>Мы в соцсетях</h5>
          <SocialIcon href="https://www.instagram.com/lovely_pet.ru/">
            <i className="fab fa-instagram"></i>
          </SocialIcon>
          <div className="pxstudio">
            разработано в{" "}
            <a className="pxstudio__link" href="https://pxstudio.pw/">
              pxstudio
            </a>
          </div>
        </div>
      </div>
    </Container>
    <style jsx>{`
      .pxstudio {
        margin-top: 2rem;
        font-family: "Montserrat";
        font-weight: 600;
      }
      .pxstudio__link {
        color: #34a29c;
        text-decoration: none;
      }
      .pxstudio__link:hover {
        opacity: 0.8;
        transition: all 0.4s ease;
      }
      .footer {
        padding: 5rem 0;
        background-color: #011e26;
        color: #fff;
      }
      .footer__content {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
      }
      .footer__column {
        flex: 1 0;
        padding: 0 1rem;
      }
      .footer :global(h5) {
        margin-top: 0;
        font-size: 18px;
        text-transform: uppercase;
      }
      .footer-menu {
        display: flex;
        flex-direction: column;
      }
      .footer-menu a {
        color: #fff;
        text-decoration: none;
        line-height: 1;
        margin-bottom: 1.5rem;
        text-transform: uppercase;
      }
      .footer-menu a:hover {
        opacity: 0.8;
        transition: all 0.2s ease;
      }
      .footer__logo {
        text-decoration: none;
        color: #fff;
        text-transform: uppercase;
        display: flex;
        align-items: center;
      }
      .footer__logo .left {
        margin-right: 0.5rem;
      }
      .footer__logo .right {
        text-align: left;
      }
      .footer__location {
        margin-top: 0.5rem;
        display: flex;
        align-items: center;
      }
      .footer__loc-icon {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 40px;
        height: 40px;
      }
      .footer__loc-phone a {
        color: #fff;
        text-decoration: none;
        font-size: 20px;
      }
      .footer__loc-phone a:hover {
        opacity: 0.8;
        transition: all 0.2s ease;
      }
      @media (max-width: 640px) {
        .footer__column {
          width: 100%;
          flex: unset;
          margin: 2rem 0;
          padding: 0;
          text-align: center;
        }
        .footer__location {
          justify-content: center;
        }
        .footer__logo {
          justify-content: center;
        }
      }
    `}</style>
  </div>
)
