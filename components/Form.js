import { useState } from "react"
import ym from "react-yandex-metrika"

const Form = () => {
  const [phone, setPhone] = useState("")
  const [email, setEmail] = useState("")
  const [isSended, setIsSended] = useState(false)
  const [err, setIsErr] = useState(false)

  const onSubmit = e => {
    e.preventDefault()

    if (phone == "" || email == "") return

    const data = {
      fields: {
        TITLE: `ЛИД С САЙТА LOVELY-PET.RU ${phone} ${email}`,
        EMAIL: [{ VALUE: email, VALUE_TYPE: "WORK" }],
        PHONE: [{ VALUE: phone, VALUE_TYPE: "WORK" }],
        SOURCE_DESCRIPTION: "Сайт lovely-pet.ru"
      }
    }

    fetch(
      "https://lovely-pet.bitrix24.ru/rest/1/45acjnjo7fjvh70t/crm.lead.add.json",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ ...data })
      }
    )
      .then(j => j.json())
      .then(d => {
        ym("reachGoal", "sended")
        setIsSended(true)
        setIsErr(false)
      })
      .catch(err => {
        setIsErr(true)
        setIsSended(false)
      })
  }

  return (
    <form className="form" onSubmit={onSubmit}>
      {!isSended && (
        <>
          <label htmlFor="email">Емэйл:</label>
          <input
            id="email"
            type="text"
            name="email"
            placeholder="Введите Ваше email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          ></input>
          <label htmlFor="phone">Номер телефона:</label>
          <input
            id="phone"
            type="phone"
            name="phone"
            value={phone}
            onChange={e => setPhone(e.target.value)}
            placeholder="Введите Ваш номер телефона"
          ></input>
          <input type="submit" value="Перезвонить" />
        </>
      )}
      {isSended && (
        <p>Ваши данные отправлены! Скоро с Вами свяжется наш специалист!</p>
      )}
      <style jsx>{`
        .form {
          display: flex;
          flex-direction: column;
        }
        .form label {
          color: #34a29c;
          text-transform: uppercase;
          margin-bottom: 0;
        }
        .form input[type="text"],
        .form input[type="phone"] {
          font: inherit;
          font-size: 18px;
          padding: 1rem 2rem;
          border: none;
          border-radius: 10px;
          box-shadow: 0px 3px 20px rgba(0, 0, 0, 0.16);
          margin-top: 0.5rem;
          margin-bottom: 1.5rem;
        }
        .form input[type="submit"] {
          font: inherit;
          padding: 1rem 2rem;
          text-transform: uppercase;
          background-color: #34a29c;
          box-shadow: 0px 3px 20px rgba(0, 0, 0, 0.16);
          border: none;
          color: #fff;
          font-size: 18px;
          border-radius: 10px;
          cursor: pointer;
          margin-top: 1rem;
        }
        .form input[type="submit"]:hover {
          opacity: 0.9;
          transition: 0.2s ease;
        }
        .form input[type="submit"]:active {
          opacity: 0.8;
          transition: 0.4s ease;
        }
      `}</style>
    </form>
  )
}

export default Form
