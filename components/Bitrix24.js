const Scripts = () => (
  <>
    <script
      type="text/javascript"
      dangerouslySetInnerHTML={{
        __html: `
          (function(w,d,u){
            var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
            var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
          })(window,document,'https://cdn.bitrix24.ru/b11608306/crm/site_button/loader_1_gj3pto.js');
      `
      }}
    />
  </>
)

export default () => {
  // return (process.env.NODE_ENV === 'production') ? <Scripts /> : ""
  return <Scripts />
}
