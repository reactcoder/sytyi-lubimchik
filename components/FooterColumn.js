export default ({ children }) => (
  <div className="footer-column">
    {children}
    <style jsx>{`
      .footer-column {
        flex: 1;
        padding: 0 1rem;
      }
      @media (max-width: 640px) {
        .footer-column {
          padding: 0;
        }
      }
    `}</style>
  </div>
)
