export default ({ children, reverse, valign = "center" }) => (
  <div>
    {children}
    <style jsx>{`
      div {
        display: flex;
        flex-direction: ${reverse ? "row-reverse" : "row"};
        justify-content: space-between;
        align-items: ${valign};
        flex-wrap: wrap;
      }
    `}</style>
  </div>
)
