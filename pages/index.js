import Page from "../components/Page"
import Header from "../components/Header"
import Container from "../components/Container"
import Hero from "../components/Hero"
import Section1 from "../components/Section1"
import Title from "../components/Title"
import Carousel from "../components/Carousel"
import CarouselItem from "../components/CarouselItem"
import Row from "../components/Row"
import CuteImage from "../components/CuteImage"
import CuteText from "../components/CuteText"
import Form from "../components/Form"
import Footer from "../components/Footer"
import CuteImage2 from "../components/CuteImage2"
import { NextSeo } from "next-seo"
import { YMInitializer } from "react-yandex-metrika"

export default () => (
  <Page>
    <YMInitializer accounts={[55216966]} options={{ webvisor: true }}>
      <NextSeo
        title="Интернет-магазин зоотоваров 'Сытый любимчик' в Ижевске | Премиум корма, витамины, лакомства для ваших домашних питомцев | Доставка по Ижевску, тел: +7 (912) 754-26-12"
        description="Большой выбор зоотоваров: сухих и влажных кормов для животных, витаминов, переносок, лакомств - в интернет-зоомагазине Сытый любимчик"
      />
      <Header />
      <Hero />
      <Section1 id="manufacturers">
        <Title subtitle="Премиум корма">
          Порадуйте своих домашних любимцев
        </Title>
        <div style={{ marginBottom: "5rem" }}>
          Мы состоим из того, что мы едим. Данный факт верен не только для нас,
          но и для наших питомцев. Заботливые владельцы «бегающих урчащих
          моторчиков» в обязательном порядке самостоятельно подбирают рацион для
          своих любимцев и в него часто входят готовые рационы кормов. Полагаясь
          на отзывы ветеринаров и покупателей, мы хотим предложить следующих
          производителей кормов.
        </div>
        <Carousel>
          <CarouselItem
            img="/static/img/royal-canin.png"
            firm="ROYAL CANIN"
            flag="/static/img/France.png"
          ></CarouselItem>
          <CarouselItem
            img="/static/img/savarra.png"
            firm="SAVARRA"
            flag="/static/img/britain.png"
          ></CarouselItem>
          <CarouselItem
            img="/static/img/orijen.png"
            firm="ORIJEN"
            flag="/static/img/canada.png"
          ></CarouselItem>
          <CarouselItem
            img="/static/img/nandd.png"
            firm="NATURAL&DELICIOUS"
            flag="/static/img/italia.png"
          ></CarouselItem>
        </Carousel>
      </Section1>
      <Section1 id="for-whom" bg="/static/img/bg.png">
        <Row>
          <CuteImage img="/static/img/kissy.jpg" />
          <CuteText>
            <Title subtitle="Для котят и кошек">Особенности</Title>
            Мы проявляем уважение к природным особенностям кошек и собак и
            опираемся на их подлинные потребности, заботясь об их здоровье и
            благополучии. Именно поэтому нам доверяют хозяева домашних животных
            по всему миру.
          </CuteText>
        </Row>
        <Row reverse>
          <CuteImage img="/static/img/doggy.jpg" />
          <CuteText>
            <Title subtitle="ДЛЯ ЩЕНКОВ И СОБАК">
              С заботой о здоровье наших питомцев
            </Title>
            Рецептуры кормов, созданные в результате многолетних научных
            исследований, регулярно обновляются, поэтому все рационы
            соответствуют высочайшим стандартам и удовлетворяют все пищевые
            потребности каждого четвероного питомца.
          </CuteText>
        </Row>
      </Section1>
      <Section1 id="form">
        <Row>
          <CuteText>
            <Title subtitle="Появились вопросы?">Задайте их нам!</Title>
            <Form></Form>
          </CuteText>
          <CuteImage2 img="/static/img/catty.jpg" />
        </Row>
      </Section1>
      <Footer id="footer"></Footer>
    </YMInitializer>
  </Page>
)
