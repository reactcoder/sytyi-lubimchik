export default ({ img, ...props }) => (
  <div className="cute-image" {...props}>
    <style jsx>{`
      .cute-image {
        margin: 1rem;
        max-width: 250px;
        width: 100%;
        max-height: 350px;
        height: 350px;
        box-shadow: 0px 3px 40px rgba(0, 0, 0, 0.3);
        background-image: url(${img});
        background-size: cover;
        background-position: center center;
        border-radius: 10px;
      }
      @media (max-width: 640px) {
        .cute-image {
          margin-left: 0;
          margin-right: 0;
          margin-top: 1rem;
          margin-bottom: 1rem;
          max-width: 100%;
        }
      }
    `}</style>
  </div>
)
