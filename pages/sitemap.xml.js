import React from "react"

const sitemapXml = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>https://lovely-pet.ru/</loc>
    <changefreq>daily</changefreq>
    <priority>1</priority>
  </url>
</urlset>`

export default class Sitemap extends React.Component {
  static getInitialProps({ res }) {
    res.setHeader("Content-Type", "text/xml")
    res.write(sitemapXml)
    res.end()
  }
}
