export default ({ img }) => (
  <div className="cute-image">
    <style jsx>{`
      .cute-image {
        position: absolute;
        bottom: -40px;
        right: -40px;
        max-width: 600px;
        width: 100%;
        max-height: 350px;
        height: 350px;
        box-shadow: 0px 3px 40px rgba(0, 0, 0, 0.3);
        background-image: url(${img});
        background-size: cover;
        background-position: center center;
        border-radius: 10px;
      }
      @media (max-width: 920px) {
        .cute-image {
          margin-left: 0;
          margin-right: 0;
          margin-top: 1rem;
          margin-bottom: 1rem;
          max-width: 300px;
        }
      }
      @media (max-width: 640px) {
        .cute-image {
          display: none;
        }
      }
    `}</style>
  </div>
)
