import Link from "next/link"

export default () => (
  <div className="footer-menu">
    <Link href="/">
      <a>Производители</a>
    </Link>
    <Link href="/">
      <a>Доставка и оплата</a>
    </Link>
    <Link href="/">
      <a>Отзывы</a>
    </Link>
    <Link href="/">
      <a>Контакты</a>
    </Link>
    <style jsx>{`
      .footer-menu {
        display: flex;
        flex-direction: column;
      }
      .footer-menu a {
        color: #fff;
        text-decoration: none;
        line-height: 2;
        text-transform: uppercase;
      }
      .footer-menu a:hover {
        opacity: 0.8;
        transition: all 0.2s ease;
      }
    `}</style>
  </div>
)
