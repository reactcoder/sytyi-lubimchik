export default ({ img, firm, flag }) => (
  <div className="carousel-item">
    <div className="carousel-item__top">
      <img src={img} alt={firm} />
    </div>
    <div className="carousel-item__bottom">
      <img src={flag} />
      <h5>{firm}</h5>
    </div>
    <style jsx>{`
      .carousel-item {
        flex: 1 0 250px;
        max-width: 260px;
        margin: 0.5rem 0.5rem 3rem 0.5rem;
        position: relative;
        padding: 0 1rem;
        box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
        border-radius: 10px;
      }
      .carousel-item:hover {
        transform: translateY(-20px);
        transition: all 0.2s ease;
        cursor: pointer;
      }
      .carousel-item__top {
        height: 230px;
        padding: 1rem;
      }
      .carousel-item__top img {
        position: absolute;
        top: -40px;
        left: 50%;
        transform: translateX(-50%);
      }
      .carousel-item__bottom {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 1rem 0.5rem;
      }
      .carousel-item__bottom img {
        margin-right: 1rem;
      }
      .carousel-item__bottom h5 {
        margin: 0;
        font-size: 17px;
      }
      @media (max-width: 640px) {
        .carousel-item {
          flex-basis: 50%;
          width: 50%;
          max-width: unset;
        }
      }
    `}</style>
  </div>
)
