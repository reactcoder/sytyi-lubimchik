import React from "react"

const str = "google-site-verification: google012dcc718ba5ff1f.html"

export default class extends React.Component {
  static getInitialProps({ res }) {
    res.setHeader("Content-Type", "text/plain")
    res.write(str)
    res.end()
  }
}
