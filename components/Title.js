export default ({ children, subtitle }) => (
  <div className="title">
    <h3 className="title__subtitle">{subtitle}</h3>
    <h2 className="title__title">{children}</h2>
    <style jsx>{`
      .title {
        max-width: 800px;
      }
      .title__subtitle {
        color: #34a29c;
        font-size: 25px;
        margin-bottom: 0.5rem;
        text-transform: uppercase;
      }
      .title__title {
        font-size: 50px;
        text-transform: uppercase;
        margin-top: 0;
      }
      @media (max-width: 640px) {
        .title__title {
          font-size: 40px;
        }
        .title__subtitle {
          font-size: 20px;
        }
      }
    `}</style>
  </div>
)
