export default ({ children }) => (
  <div className="carousel">
    {children}
    <style jsx>{`
      .carousel {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
      }
    `}</style>
  </div>
)
