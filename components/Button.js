import Link from "next/link"

export default ({ href, children, onClick }) => (
  <>
    <Link href={href}>
      <a className="btn" onClick={onClick}>
        {children}
      </a>
    </Link>
    <style jsx>{`
      .btn {
        font: inherit;
        color: #34a29c;
        background-color: #fff;
        text-transform: uppercase;
        padding: 0.8rem 2rem;
        border: none;
        font-size: 25px;
        border-radius: 50px;
        box-shadow: 0px 3px 10px rgba(0, 0, 0, 0.4);
        cursor: pointer;
        overflow: hidden;
        transition: all 0.3s ease-in;
        text-decoration: none;
      }
      .btn:hover {
        transform: translateY(-20px);
        transition: all 0.2s ease;
      }
      @media (max-width: 640px) {
        .btn {
          font-size: 18px;
        }
      }
    `}</style>
  </>
)
