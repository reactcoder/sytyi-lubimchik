import Container from "./Container"

export default ({ id, children, bg }) => (
  <div className="section" id={id}>
    <Container>{children}</Container>
    <style jsx>{`
      .section {
        position: relative;
        padding: 5rem 0;
        background-image: url(${bg});
        background-size: cover;
        background-position: center center;
        overflow: hidden;
      }
    `}</style>
  </div>
)
