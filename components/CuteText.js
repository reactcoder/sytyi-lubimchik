export default ({ children }) => (
  <div>
    {children}
    <style jsx>{`
      div {
        max-width: 600px;
      }
    `}</style>
  </div>
)
