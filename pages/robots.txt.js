import React from "react"

const robotsTxt = `User-agent: *
Allow: /
Sitemap: https://lovely-pet.ru/sitemap.xml`

export default class AdsTxt extends React.Component {
  static getInitialProps({ res }) {
    res.setHeader("Content-Type", "text/plain")
    res.write(robotsTxt)
    res.end()
  }
}
