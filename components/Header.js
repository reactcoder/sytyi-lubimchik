import { useState } from "react"
import Link from "next/link"
import Container from "./Container"

class Header extends React.Component {
  state = {
    mobileMenu: false,
    scrolled: false
  }

  onScroll = e => {
    // lastScrollY = window.scrollY

    if (window.scrollY > 200) {
      this.setState({ scrolled: true })
    } else {
      this.setState({ scrolled: false })
    }
    // if (e.target.scrollTop > 100) {
    //   this.setState({ scrolled: true })
    // }
    console.log(this.state.scrolled)
  }

  componentDidMount() {
    window.addEventListener("scroll", this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll)
  }

  render() {
    const { mobileMenu, scrolled } = this.state
    return (
      <header className={`header ${scrolled ? "header_scrolled" : ""}`}>
        <Container>
          <div className="header__content">
            <Link href="/">
              <a className="header__logo">
                <div className="left">
                  <img src="/static/logo.png" alt="логотип 'Сытый любимчик'" />
                </div>
                <div className="right">
                  <div>Сытый</div>
                  <div>любимчик</div>
                </div>
              </a>
            </Link>
            <ul className={`header__menu ${mobileMenu ? "mobile" : ""}`}>
              <li>
                <Link href="#manufacturers">
                  <a>Производители</a>
                </Link>
              </li>
              <li>
                <Link href="#for-whom">
                  <a>Для кого</a>
                </Link>
              </li>
              <li>
                <Link href="#reviews">
                  <a>Отзывы</a>
                </Link>
              </li>
              <li>
                <Link href="#footer">
                  <a>Контакты</a>
                </Link>
              </li>
            </ul>
            <button
              className="bars"
              onClick={() =>
                this.setState(({ mobileMenu }) => {
                  return {
                    mobileMenu: !mobileMenu
                  }
                })
              }
            >
              <i className="fas fa-bars"></i>
            </button>
          </div>
        </Container>
        <style jsx>{`
          .header {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            padding: 2rem 0;
            z-index: 2000;
          }
          .header_scrolled {
            background-color: #34a29c;
            padding: 0.5rem 0;
            transition: all 0.5s ease-in;
            box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
          }
          .header__logo {
            text-transform: uppercase;
            font-size: 20px;
            color: #fff;
            text-decoration: none;
            line-height: 1;
            display: flex;
            align-items: center;
          }
          .header__logo .left {
            margin-right: 0.5rem;
          }
          .header__content {
            display: flex;
            justify-content: space-between;
            align-items: center;
          }
          .header__menu {
            display: flex;
            list-style-type: none;
            margin: 0;
            padding: 0;
          }
          .header__menu li {
            padding-left: 1rem;
          }
          .header__menu a {
            text-transform: uppercase;
            color: #fff;
            text-decoration: none;
            line-height: 1.5;
          }
          .header .header__menu a:hover {
            border-bottom: 3px solid #34a29c;
          }
          .header.header_scrolled .header__menu a:hover {
            border: none;
            opacity: 0.8;
            transition: 0.2s ease;
          }
          .bars {
            display: none;
            width: 30px;
            height: 30px;
            background-color: transparent;
            border: none;
            color: #fff;
            justify-content: center;
            align-items: center;
            font-size: 20px;
            cursor: pointer;
          }
          @media (max-width: 920px) {
            .header {
              padding: 1rem 0;
            }
            .header__menu {
              display: none;
            }
            .bars {
              display: flex;
            }
            .header__menu.mobile {
              position: absolute;
              display: flex;
              flex-direction: column;
              padding: 1rem;
              top: 4rem;
              right: 2rem;
              background-color: #fff;
              border-radius: 10px;
              color: #000;
              box-shadow: 0px 3px 20px rgba(0, 0, 0, 0.2);
            }
            .header__menu.mobile a {
              color: #34a29c;
              font-size: 18px;
            }
            .header__menu.mobile li {
              padding-left: 0;
              margin-top: 1rem;
              margin-bottom: 1rem;
            }
          }
        `}</style>
      </header>
    )
  }
}

export default Header
