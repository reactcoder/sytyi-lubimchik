const Page = ({ children }) => {
  return (
    <div className="page">
      {children}
      <style jsx>{`
        .page {
          overflow: hidden;
        }
      `}</style>
      <style jsx global>{`
        @font-face {
          font-family: "BahnschriftR";
          font-style: normal;
          font-weight: normal;
          src: url("/static/fonts/bahnschrift.ttf") format("truetype");
        }

        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }
        body {
          font-family: "BahnschriftR", sans-serif;
          font-size: 16px;
          font-weight: 300;
          color: rgba(0, 0, 0, 0.64);
          padding: 0;
          margin: 0;
          transition: all 0.2s ease;
        }
      `}</style>
    </div>
  )
}

export default Page
