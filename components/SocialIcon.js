export default ({ href, children }) => (
  <a className="social__icon" href={href}>
    {children}
    <style jsx>{`
      .social__icon {
        background-color: #fff;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        text-decoration: none;
        box-shadow: 0px 3px 20px rgba(0, 0, 0, 0.16);
        font-size: 22px;
        color: #011e26;
        cursor: pointer;
      }
      .social__icon:hover {
        transform: translateY(-4px);
      }
    `}</style>
  </a>
)
